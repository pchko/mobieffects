import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Modules
import { AppRoutingModule } from './app-routing.module';
import { MoviesModule } from './components/movies/movies.module';
import { ShipsModule } from './components/ships/ships.module';

//Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';




@NgModule({
  declarations: [
    HomeComponent,
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MoviesModule,
    ShipsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
