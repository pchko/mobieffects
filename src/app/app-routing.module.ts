import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './components/movies/movies.component';
import { HomeComponent } from './components/home/home.component';
import { ShipsComponent } from './components/ships/ships.component';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent
	},
	{
    	path: 'home',
    	component: HomeComponent
	},
	{
    	path: 'movies',
    	component: MoviesComponent
	},
	{
    	path: 'ships/:idFilm',
    	component: ShipsComponent
	}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
