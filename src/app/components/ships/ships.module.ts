import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShipsComponent } from './ships.component';


@NgModule({
  declarations: [
  	ShipsComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
  	ShipsComponent
  ]
})
export class ShipsModule { }
