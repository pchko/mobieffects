import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.sass']
})
export class MoviesComponent implements OnInit {

	movies:any;

	constructor(private _api:ApiService){ }

	ngOnInit() {
		this._api.getMovies().then( (response:any) => {
			this.movies = response.results;
			console.log(this.movies);
		}, (error) => {
			console.log(error);
  		});
  	}
}
