import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { MoviesComponent } from './movies.component';

@NgModule({
  declarations: [
  	MoviesComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule
  ],
  exports:[
  	MoviesComponent
  ]
})
export class MoviesModule { }
