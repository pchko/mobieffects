import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

	baseUrl:string;

	constructor(private _http: HttpClient) {
		this.baseUrl = environment.apiUrl;
  	}

  	public getMovies() {
  		let promise = new Promise( (resolve, reject) => {
		  	let apiEndpoint = `${this.baseUrl}/films`;

		  	let response = this._http.get(apiEndpoint)
	    	.toPromise()
	    	.then(
		      	res => { // Success
		        	resolve(res);
		      	},
		      	msg => { // Error
		        	reject(msg);
		      	}
	    	).catch(
	      		error => catchError(this.handleError('Get configurations', []))
	    	);

		});

		return promise;
  	}

  	private handleError(operation = 'operation', result?: any) {
    	return (error: any): Observable<any> => {
      		console.error(error);
      		this.log(`${operation} failed: ${error.message}`);
      		return of(result as any);
    	};
  	}

  	private log(message: string) {
    	console.log(message);
  	}
}
